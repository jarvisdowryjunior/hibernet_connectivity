package com.customerApplication.customerApplication.service;


import com.customerApplication.customerApplication.dao.CustomerDAO;
import com.customerApplication.customerApplication.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;
//    private int customerId = 1;
//    private List<Customer> customerList = new CopyOnWriteArrayList<>();

    public Customer addCustomer(Customer customer){
//        customer.setCustomerId(customerId);
//        customerList.add(customer);
//        customerId += 1;
//        return customer;
        return customerDAO.save(customer);
    }

    public List<Customer> getCustomer(){
        return customerDAO.findAll();
//        return customerList;
    }

    public Customer getCustomer(int customerId){
        return customerDAO.findById(customerId).get();
//        return customerList
//                .stream()
//                .filter(c -> c.getCustomerId() == customerId)
//                .findFirst()
//                .get();
    }

//    public Customer updateCustomer(int id,Customer customer){
//        customerList
//                .stream()
//                .forEach(c -> {
//                    if(c.getCustomerId()==id){
//                        c.setCustomerFirstName(customer.getCustomerFirstName());
//                        c.setCustomerLastName(customer.getCustomerLastName());
//                        c.setCustomerEmail(customer.getCustomerEmail());
//                    }
//
//                });
//        return customerList
//                .stream()
//                .filter(c -> c.getCustomerId()==id)
//                .findFirst()
//                .get();
//    }

    public void deleteCustomer(int id){
        customerDAO.deleteById(id);
//        customerList
//                .stream()
//                .forEach(c -> {
//                    if( c.getCustomerId() == id){
//                        customerList.remove(c);
//                    }
//                });
    }

}


